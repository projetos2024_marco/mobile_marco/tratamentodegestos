import React from "react";
import CountMoviment from "./src/countMoviment";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import ImageMoviment from "./src/imageMoviment";
import HomeScreen from "./src/homeScreen";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="CountMoviment" component={CountMoviment} />
        <Stack.Screen name="ImageMoviment" component={ImageMoviment} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
